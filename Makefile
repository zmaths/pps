

all: presentation pdfpc


presentation: *.tex
	pdflatex presentation.tex

pdfpc: presentation.pdfpc
	sed -i "s/\\\\\\\\/\n/g" presentation.pdfpc
	sed -i "s/\\\\par/\n\n/g" presentation.pdfpc

.PHONY: yon

yon:
	git pull http://bardinflor.perso.aquilenet.fr/pps.git